<?php

namespace phpstd;

function get_type() {
    return call_user_func_array('gettype',func_get_args());
}

function php_info() {
    return phpinfo();
}
